const express = require('express');
const { ApolloServer, gql, graphiqlExpress } = require('apollo-server-express');
const corsMiddleware = require('cors');
const typeDefs = require('./typeDefs');
const resolvers = require('./resolvers');
//const loaders = require('./loaders');
const { findAuthorsByBookIdsLoader } = require('./authors');
const { findBooksByIdsLoader } = require('./book');
const { findUsersByIdsLoader } = require('./users');

const schema = new ApolloServer({
  typeDefs,
  resolvers,
  context: {
    loaders: {
      findAuthorsByBookIdsLoader: findAuthorsByBookIdsLoader(),
      findBooksByIdsLoader: findBooksByIdsLoader(),
      findUsersByIdsLoader: findUsersByIdsLoader(),
    },
  },
});

//Acts like a commandline app
/* const query = process.argv[2];
graphql(schema, query).then((result) => {
  console.log(JSON.stringify(result, null, 2));
}); */

//invoking an express server

const app = express();
app.use(corsMiddleware());
/* app.use('/graphql', bodyParserMiddleware.json(), graphqlExpress({ schema }));
console.log('fine');
app.use('graphiql', graphiqlExpress({ endpointURL: '/graphql' })); */

schema.applyMiddleware({ app });

app.listen(4000, () =>
  console.log('go to http://localhost:4000/graphql to run queries', 4000)
);
