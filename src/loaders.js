const { findAuthorsByBookIdsLoader } = require('./authors');

module.exports = () => ({
  findAuthorsByBookIdsLoader: findAuthorsByBookIdsLoader(),
});
