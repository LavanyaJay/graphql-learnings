const { query } = require('./db');
var _ = require('lodash');
const DataLoader = require('dataloader');

async function allBooks(args) {
  const ORDER_BY = {
    ID_DESC: 'id desc',
    RATING_DESC: 'rating desc',
  };

  const orderBy = ORDER_BY[args.orderBy];
  const sql = `
  select * from hb.book
  order by ${orderBy}
  `;
  try {
    const result = await query(sql);
    return result.rows;
  } catch (err) {
    console.log(err);
    throw err;
  } finally {
  }
}
function imageUrl(size, id) {
  const zoom = size === 'SMALL' ? 1 : 0;
  return `//books.google.com/books/content?id=${id}&printsec=frontcover&img=1&zoom=${zoom}&source=gbs_api`;
}

async function findBookById(id) {
  const sql = `
     select *
     from hb.book
     where hb.book.id = $1
   `;
  const params = [id];
  try {
    const result = await query(sql, params);
    return result.rows[0];
  } catch (err) {
    console.log(err);
    throw err;
  }
}

async function findBooksByIds(ids) {
  const sql = `
     select *
     from hb.book
     where hb.book.id = ANY ($1)
   `;
  const params = [ids];
  try {
    const result = await query(sql, params);
    const rowsById = _.groupBy(result.rows, 'id');
    return _.map(ids, (id) => {
      const book = rowsById[id] ? rowsById[id][0] : null;
      return book;
    });
  } catch (err) {
    console.log(err);
    throw err;
  }
}
function findBooksByIdsLoader() {
  return new DataLoader(findBooksByIds);
}

module.exports = { allBooks, imageUrl, findBookById, findBooksByIdsLoader };
