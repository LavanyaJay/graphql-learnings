const { gql } = require('apollo-server-express');
const typeDefs = `
  schema {
    query: Query
  }
  type Query {
    books(orderBy:BooksOrderBy=RATING_DESC): [Book]
    reviews(orderBy:ReviewsOrderBy=ID_DESC):[Review]
    
  }
  type Book {
    id: ID!
    title: String!
    subtitle:String!
    description: String!
    imageUrl(size:ImageSize=LARGE):String! 
    ratingCount: Int!
    rating: Float
    authors:[Author]
   
    
  }
  type Author {
    id :ID!,
    name: String!
  }
  type Review {
    id: ID!
    rating: Int
    title: String
    comment:String
    book:Book!
    user:User!
  }
  enum ImageSize {
    SMALL
    LARGE
  }
  enum BooksOrderBy{
    RATING_DESC
    ID_DESC
    
  }
  enum ReviewsOrderBy{
    ID_ASC
    ID_DESC
  }
  type User {
    id:ID!
    email:String
    name:String
  }
`;
module.exports = typeDefs;
