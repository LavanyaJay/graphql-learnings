const { query } = require('./db');
const DataLoader = require('dataloader');
var _ = require('lodash');
const { groupBy, map } = require('ramda');

async function findAuthorsByBookIds(ids) {
  const sql = `
  select 
  hb.author.*,
  hb.book_author.book_id
  from hb.author inner join hb.book_author
    on hb.author.id = hb.book_author.author_id
  where hb.book_author.book_id = ANY($1);
  `;
  const params = [ids];
  try {
    const result = await query(sql, params);

    const rowsById = _.groupBy(result.rows, (author) => author.book_id);

    return _.map(ids, (id) => {
      return rowsById[id];
    });
  } catch (err) {
    console.log(err);
    throw err;
  } finally {
  }
}
async function authorsByBookId(id) {
  const sql = `
   select
   hb.author.*,
   hb.book_author.book_id
   from hb.author inner join hb.book_author
   on hb.author.id = hb.book_author.author_id
   where hb.book_author.book_id = $1
   `;
  const params = [id];
  try {
    const result = await query(sql, params);
    return result.rows;
  } catch (err) {
    console.log(err);
    throw err;
  } finally {
  }
}
function findAuthorsByBookIdsLoader() {
  return new DataLoader(findAuthorsByBookIds);
}
module.exports = {
  authorsByBookId,
  findAuthorsByBookIds,
  findAuthorsByBookIdsLoader,
};
