const { allBooks, imageUrl } = require('./book');
const { allReviews } = require('./review');

const resolvers = {
  Query: {
    books: (root, args) => {
      return allBooks(args);
    },
    reviews: (root, args) => {
      return allReviews(args);
    },
  },
  Book: {
    ratingCount: (book) => {
      return book.rating_count;
    },
    imageUrl: (book, { size }) => {
      return imageUrl(size, book.google_id);
    },
    authors: (book, args, context) => {
      const { loaders } = context;
      const { findAuthorsByBookIdsLoader } = loaders;
      return findAuthorsByBookIdsLoader.load(book.id);
    },
  },
  Review: {
    book: (review, args, context) => {
      const { loaders } = context;
      const { findBooksByIdsLoader } = loaders;
      return findBooksByIdsLoader.load(review.book_id);
      //return findBookById(review.book_id);
    },
    user: (review, args, context) => {
      const { loaders } = context;
      const { findUsersByIdsLoader } = loaders;
      return findUsersByIdsLoader.load(review.user_id);
    },
  },
};
module.exports = resolvers;
