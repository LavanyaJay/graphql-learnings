const { Pool } = require('pg');
const humps = require('humps');
const pool = new Pool({
  host: 'localhost',
  database: 'hackerbook',
});
function logQuery(sql, params) {
  console.log('BEGIN-------------------------------------');
  console.log('SQL:', sql);
  console.log('PARAMS:', JSON.stringify(params));
  console.log('END---------------------------------------');
}
async function query(sql, params) {
  const client = await pool.connect();
  logQuery(sql, params);
  try {
    return client.query(sql, params);
  } catch (err) {
    console.log(err);
  } finally {
    client.release();
  }
}

module.exports = { query, logQuery };
