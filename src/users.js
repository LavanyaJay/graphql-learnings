const { query } = require('./db');
var _ = require('lodash');
const DataLoader = require('dataloader');

async function findUsersByIds(ids) {
  const sql = `
     select *
     from hb.user
     where hb.user.id = ANY ($1)
   `;

  const params = [ids];

  try {
    const result = await query(sql, params);
    console.log(result);
    const rowsById = _.groupBy(result.rows, 'id');
    console.log(rowsById);
    console.log(
      _.map(ids, (id) => {
        return rowsById[id];
      })
    );
    return _.map(ids, (id) => {
      const user = rowsById[id] ? rowsById[id][0] : null;
      return user;
    });
  } catch (err) {
    console.log(err);
    throw err;
  }
}

function findUsersByIdsLoader() {
  return new DataLoader(findUsersByIds);
}
module.exports = { findUsersByIdsLoader };
